# Marple Plugin Tester
Use this repository to test and develop plugins for Marple

## Installation
1. Clone this repository to a local folder

2. Make sure that you have python 3.7 (or later) installed

3. Extra packages:
    - click (`pip install click`)

## Usage
Ideally you put a data file and the plugin in the same folder as the plugin tester.

Next, in the terminal:

`python test_plugin.py`

The script will ask you to enter the plugin name and data file name.

Optionally you can provide the inputs as a oneliner already. (Useful when you are iteratively testing the plugin)

`python test_plugin.py --plugin_name example_csv_plugin --example_data example_v14_8.csv`


The script will guide you to create a plugin for your data. The output of the script is printed in the terminal. Make sure to check if the output matches what you expect.

Enjoy!
