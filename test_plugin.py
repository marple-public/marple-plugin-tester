import os
import click
from importlib import import_module
import inspect

NUM_PARAMS_REQUIRED = {
    'get_headers': 1,
    'write_values': 2,
    'get_metadata': 1
}

MAX_PRINT_HEADERS = 30
MAX_PRINT_ROWS = 10
MAX_PRINT_LENGTH = 110
printed_rows = 0


@click.command()
@click.option('--plugin_name', prompt='Plugin name? ', help='Plugin name')
@click.option('--example_data', prompt='Example data name? ', help='Example data name')
def main(plugin_name, example_data):
    plugin_name = plugin_name.replace('.py', '').replace(os.sep, '.')
    plugin_module = import_module(plugin_name)
    print('=== Checking plugin ===')

    print('\nCheck plugin elements..')
    if not check_required_elements(plugin_module):
        raise Exception('Invalid plugin')

    print('\nChecking get_headers..')
    if not check_headers(plugin_module, example_data):
        raise Exception('Invalid get_headers')

    print('\nChecking get_metadata..')
    if not check_meta_data(plugin_module, example_data):
        raise Exception('Invalid get_metadata')

    print('\nChecking write_values..')
    if not check_data(plugin_module, example_data):
        raise Exception('Invalid write_values')

    print('\n=== Plugin OK ===')


def check_required_elements(plugin_module):
    plugin_functions_names = [i[0] for i in inspect.getmembers(plugin_module, inspect.isfunction)]
    required_function_names = NUM_PARAMS_REQUIRED.keys()
    missing_functions = [f for f in required_function_names if f not in plugin_functions_names]
    for f in missing_functions:
        print('Missing function in plugin: {}'.format(f))
    return missing_functions == []


def check_headers(plugin_module, example_data):
    header_info = plugin_module.get_headers(example_data)
    if not isinstance(header_info, list):
        print('get_headers does not return a list')
        return False
    if header_info == []:
        print('get_headers returns an empty list')
        return False
    if 'name' not in header_info[0]:
        print("get_headers should return items with key:name in it (eg. [{'name': 'time'}])")
        return False

    for item in header_info:
        if 'groups' in item:
            if not isinstance(item['groups'], list):
                print("Groups should be a list of strings. {} was given instead".format(item['groups']))
                return False

    headers = [e['name'] for e in header_info]

    dupe_headers = [h for h in headers if (headers.count(h) > 1 and h != 'time')]
    if len(dupe_headers) > 0:
        print(f"Warning: duplicate headers will be suffixed to ensure uniqueness: {dupe_headers}")

    time_bases = {hi.get('time_base', 0) for hi in header_info}
    print_headers(headers, time_bases)

    if 'time' not in headers:
        print("No 'time' header found")
        return False

    return True


def print_headers(headers, time_bases):
    print('Received {} headers on {} time bases'.format(len(headers), len(time_bases)))
    print('Printing first {} headers'.format(MAX_PRINT_HEADERS))
    print(' - ' + '\n - '.join(headers[:MAX_PRINT_HEADERS]))
    if len(headers) > MAX_PRINT_HEADERS:
        print(' ... {} more headers (not shown)'.format(len(headers) - MAX_PRINT_HEADERS))


def check_meta_data(plugin_module, example_data):
    metadata_info = plugin_module.get_metadata(example_data)
    if not isinstance(metadata_info, dict):
        print('get_metadata does not return a dictionary')
        return False

    print('Received metadata')
    for k, v in metadata_info.items():
        print(' - {} = {}'.format(k, v))

    return True


def check_data(plugin_module, example_data):
    try:
        plugin_module.write_values(write_to_db_mock, example_data)
    except StopIteration:
        return True
    return True


def write_to_db_mock(values, time_base=0, progress=1):
    global printed_rows
    if printed_rows >= MAX_PRINT_ROWS:
        raise StopIteration

    print('Printing first {} data rows'.format(MAX_PRINT_ROWS))
    rows_to_print = min(len(values), MAX_PRINT_ROWS - printed_rows)
    for i in range(rows_to_print):
        print(format_row(values[i]))
    printed_rows += rows_to_print

    check_shape(values)


def format_row(row):
    raw_string = ' - ' + ', '.join([str(e) for e in row])

    if len(raw_string) > MAX_PRINT_LENGTH:
        return raw_string[:MAX_PRINT_LENGTH] + '...'

    return raw_string


def check_shape(values):
    if len(values.shape) > 2:
        error_message = 'Error: shape of data supplied to write_to_db is larger than 2: got {}'
        raise Exception(error_message.format(values.shape))

    elif len(values.shape) == 1:
        print('Warning: you are possibly using nested 1D arrays instead of 2D array')
        num_rows = values.shape[0]
        num_sigs = values[0].shape[0]

    elif len(values.shape) == 0:
        raise Exception('Error: empty data set is supplied to write_to_db')

    else:  # 2D
        (num_rows, num_sigs) = values.shape

    if num_rows < num_sigs:
        error_message = 'Warning: supplied less rows ({}) than signals ({} columns) to write_to_db. Is your data upside down?'  # noqa
        print(error_message.format(num_rows, num_sigs))


if __name__ == '__main__':
    main()
