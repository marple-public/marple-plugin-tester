# This is an example plugin to import csv data into Marple
# It is regular python (v3.7+), so anything you can do in python, you can do here
# Necessary libraries can be installed on your Marple server

import pandas as pd
import os

PLUGIN_NAME = 'CSV PARSER'
CHUNK_SIZE = int(1e3)


# REQUIRED FUNCTIONS

# Returns the names of the headers/signals that are present in the file
def get_headers(file_path):
    df = pd.read_csv(file_path, nrows=1)
    return [{'name': header} for header in df.keys()]


# Writes the values to the Marple database, one chunk at a time
# Progress (optional, 0-100) will show a progress bar in Marple
def write_values(write_to_db, file_path):
    total_chunks = get_total_chunks(file_path)
    df = pd.read_csv(file_path, chunksize=CHUNK_SIZE)

    for index, chunk in enumerate(df):
        progress = index / total_chunks * 100
        write_to_db(chunk.values, progress=progress)


# Returns the metadata. Metadata from an external place can be fetched with een API call
def get_metadata(file_path):
    return {
        'prototype_version': file_path.split('_')[1],
    }


# AUXILIARY FUNCTIONS

def get_total_chunks(file_path):
    header_amount = len(get_headers(file_path))
    file_size = os.stat(file_path).st_size
    num_rows_estimate = file_size / (header_amount * 8)
    return num_rows_estimate / CHUNK_SIZE
